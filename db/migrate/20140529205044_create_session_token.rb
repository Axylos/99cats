class CreateSessionToken < ActiveRecord::Migration
  def change
    create_table :session_tokens do |t|
      t.string :token, null: false
      t.integer :user_id, null: false
    end
    
    add_index :session_tokens, :user_id
    add_index :session_tokens, :token, unique: true
  end
end
