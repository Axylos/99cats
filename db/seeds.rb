# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Cat.create!(name: "Fluffy", age: 2, sex: "F", color: "black", birth_date: "01/05/2012", user_id: 1)
Cat.create!(name: "Jonathan", age: 11, sex: "M", color: "green", birth_date: "04/09/1999", user_id: 1)
Cat.create!(name: "James", age: 3, sex: "M", color: "orange", birth_date: "04/09/1899", user_id: 2)

CatRentalRequest.create!(cat_id: 2, status: "APPROVED", start_date: "01/01/2010",
                          end_date: "03/01/2010", user_id: 1)
                          
CatRentalRequest.create!(cat_id: 2, status: "PENDING", start_date: "02/01/2011",
                        end_date: "04/01/2011", user_id: 2)
                        
CatRentalRequest.create!(cat_id: 1, status: "APPROVED", start_date: "02/01/2010",
                          end_date: "03/01/2010", user_id: 2)
                          
CatRentalRequest.create!(cat_id: 3, status: "PENDING", start_date: "02/01/2012",
                          end_date: "03/01/2012", user_id: 1)
                          
CatRentalRequest.create!(cat_id: 3, status: "PENDING", start_date: "02/01/2012",
                        end_date: "03/01/2012", user_id: 1)
                        
CatRentalRequest.create!(cat_id: 3, status: "PENDING", start_date: "02/01/2012",
                        end_date: "03/01/2012", user_id: 2)

drake = User.new(user_name: "Drake")
drake.password = "stuffy"
drake.save!

arieh = User.new(user_name: "Arieh")
arieh.password = "drakey"
arieh.save!