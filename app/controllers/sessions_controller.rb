class SessionsController < ApplicationController
  before_action :redirect_if_logged_in, only: [:new]
  
  def new
  end
  
  def create
    @user = User.find_by_credentials(user_params[:user_name], user_params[:password])
    if @user.nil?
      flash.now[:notice] = "Invalid credentials"
      render :new
    else
      sign_in(@user)
      redirect_to cats_url
    end
  end
  
  def destroy
    sign_out!
    redirect_to new_session_url
  end
  
  private
  
  def user_params
    params.require(:user).permit(:user_name, :password)
  end
  
  def redirect_if_logged_in
    if signed_in?
      redirect_to cats_url
    end
  end
end
