class CatsController < ApplicationController
  before_action :owner_check, only: [:edit, :update]
  
  def index
    @cats = Cat.all
  end
  
  def show
    @user_id = current_user.try(:id) || 0
    @cat = Cat.find(params[:id])
  end
  
  def edit
    @cat = Cat.find(params[:id])
  end
  
  def update
    @cat = Cat.find(params[:id])
    @cat.update(cat_params)
    redirect_to cat_url(@cat)
  end
  
  def new
    @cat = Cat.new
  end
  
  def create
    @cat = Cat.create!(cat_params)
    @cat.user_id = current_user.id
    redirect_to cat_url(@cat)
  end
  
  private
  
  def owner_check
    if !current_user || Cat.find(params[:id]).user_id != current_user.id
      flash[:notice] = "You don't own that cat."
      redirect_to cats_url 
    end
  end
  
  def cat_params
    params.require(:cat).permit(:age, :sex, :birth_date, :name, :color)
  end
end