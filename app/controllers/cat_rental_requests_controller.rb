class CatRentalRequestsController < ApplicationController
  before_action :owner_check, only: [:approve, :deny]
  before_action :check_login, only: [:create, :new]
  
  def new
    @request = CatRentalRequest.new
  end
  
  def create
    @request = current_user.cat_rental_requests.new(request_params)
    @request.save!
    redirect_to cat_url(@request.cat_id)
  end
  
  def approve
    @request = CatRentalRequest.find(request_params[:id]).approve!
    redirect_to cat_url(@request.cat_id)
  end
  
  def deny
    @request = CatRentalRequest.find(request_params[:id]).deny!
    redirect_to cat_url(@request.cat_id)
  end
  
  private
  
  def owner_check
    if CatRentalRequest.find(request_params[:id]).cat.owner != current_user
      flash[:notice] = "You don't own that cat."
      redirect_to cats_url 
    end
  end

  def request_params
    params.require(:request).permit(:id, :start_date, :end_date)
  end
  
  def check_login
    unless signed_in?
      flash[:notice] = "Not logged in!"
      redirect_to new_session_url
    end
  end
end
