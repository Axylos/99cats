class UsersController < ApplicationController
  before_action :redirect_logged_in, only: [:new]
  
  def new
    @user = User.new
  end
  
  def index
    @users = User.all
  end
  
  def create
    @user = User.new(user_params)
    @user.password = user_params[:password]
    @user.save!
    sign_in(@user)
    redirect_to cats_url
  end
  
  private
  
  def user_params
    params.require(:user).permit(:user_name, :password)
  end
  
  def redirect_logged_in
    if signed_in?
      redirect_to cats_url
    end
  end
  
end
