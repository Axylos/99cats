module SessionsHelper
  
  def current_user
    return nil unless session[:token]
    @current_user ||= SessionToken.find_by_token(session[:token]).user
  end
  
  def sign_in(user)
    @current_user = user
    
    new_token = SessionToken.create!(token: generate_token, user_id: @current_user.id)
    session[:token] = new_token.token
  end
  
  def generate_token
    SecureRandom::urlsafe_base64(16)
  end

  def signed_in?
    current_user.present?
  end
  
  def sign_out!
    current_user.try(:reset_session_token)
    SessionToken.find_by_token(session[:token]).destroy!
    session[:token] = nil
  end
  
end
