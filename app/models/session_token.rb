class SessionToken < ActiveRecord::Base
  belongs_to :user
  validates :token, :user_id, presence: true
  validates :token, uniqueness: true
end
