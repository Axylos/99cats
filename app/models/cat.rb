class Cat < ActiveRecord::Base
  belongs_to :owner, :class_name => "User", :foreign_key => :user_id
  
  CAT_COLORS = %w{black orange red yellow green blue purple calico white}
  
  validates :age, :birth_date, :color, :name, :sex, presence: true
  validates :age, numericality: true
  validates :color, inclusion: {in: CAT_COLORS}
  validates :sex, inclusion: {in: %w{M F}}
  
  has_many :requests, class_name: "CatRentalRequest", dependent: :destroy
  
  def self.colors
    CAT_COLORS
  end
end