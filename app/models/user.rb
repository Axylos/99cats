class User < ActiveRecord::Base
  attr_reader :password
  validates :password_digest, :presence => { :message => "Password can't be blank"}
  validates :password, :length => { :minimum => 6, allow_nil: true  }
  
  has_many :cats
  has_many :cat_rental_requests
  has_many :session_tokens
  
  before_validation do
    self.session_token ||= User.generate_token
  end
  
  def self.generate_token
    SecureRandom::urlsafe_base64(16)
  end
  
  def reset_session_token!
    self.session_token = User.generate_token
    self.save!
    self.session_token
  end
  
  def password=(password)
    @password = password
    self.password_digest = BCrypt::Password.create(password)
  end
  
  def is_password?(password)
    BCrypt::Password.new(self.password_digest).is_password?(password)
  end
  
  def self.find_by_credentials(user_name, password)
    user = User.find_by_user_name(user_name)
    
    user if user.is_password?(password) 
  end
  
end