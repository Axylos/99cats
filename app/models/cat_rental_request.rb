class CatRentalRequest < ActiveRecord::Base
  validates :status, inclusion: { in: %w{PENDING APPROVED DENIED} }
  validates :status, :start_date, :end_date, :cat_id, :user_id, presence: true
  validate :overlapping_approved_requests
  belongs_to :cat
  belongs_to :user
  
  before_validation(on: :create) do
    self.status ||= "PENDING"
  end
  
  def approve!
    self.status = "APPROVED"
    
    ActiveRecord::Base.transaction do
      self.save!
      (overlapping_pending_requests).update_all("status = 'DENIED'")
    end
    self
  end
  
  def deny!
    self.status = "DENIED"
    self.save!
    self
  end
  
  def overlapping_pending_requests
    CatRentalRequest.where("
      NOT ( (TO_DATE (:start, 'yyyy-mm-dd') > end_date) OR
          (start_date > TO_DATE (:last, 'yyyy-mm-dd') ) )
      
      AND cat_id = :cat_id
      
      AND id != :self_id", { start: self.start_date, last: self.end_date, 
                              cat_id: self.cat_id, self_id: self.id } )
  end
  
  def overlapping_approved_requests
    overlapping_approved = overlapping_pending_requests.where("status = 'APPROVED'")

    unless overlapping_approved.empty?
      errors[:base] << "There's an overlapping request!"
    end
  end
    
  def pending?
    self.status == "PENDING"
  end
end
